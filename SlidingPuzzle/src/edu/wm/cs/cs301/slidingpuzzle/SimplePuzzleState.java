/**
 * 
 */
package edu.wm.cs.cs301.slidingpuzzle;
import java.util.*;
/**
 * @author Yiru Zhang <yzhang58@email.wm.edu>
 *
 */


public class SimplePuzzleState implements PuzzleState {

	private SimplePuzzleState parentState;
	private Operation op;
	private int[][] curState = null;
	private int pathLen;
	
	@Override
	/** setToInitialState sets the parent and op to null and 
	 *  sets all tiles to the same positions as the final state slots.
	 *  Empty slots are denotated as 0.
	 */ 
	public void setToInitialState(int dimension, int numberOfEmptySlots) {
		// TODO Auto-generated method stub
		// int totalSlots = dimension * dimension - numberOfEmptySlots;
		this.parentState = null;
		this.op = null;
		this.curState = new int[dimension][dimension];
		this.pathLen = 0;
		
		int cur = 1, k = dimension - 1;
		for (int i = 0; i < dimension; ++i) {
			for (int j = 0; j < dimension; ++j) {
				curState[i][j] = cur++;
			}
		}
		for (int i = 0; i < numberOfEmptySlots; ++i) {
			curState[dimension - 1][k--] = 0;
		}
		
	}

	@Override
	public int getValue(int row, int column) {
		// TODO Auto-generated method stub
		return curState[row][column];
	}

	@Override
	public PuzzleState getParent() {
		// TODO Auto-generated method stub
		return this.parentState;
	}

	@Override
	public Operation getOperation() {
		// TODO Auto-generated method stub
//		System.out.println("operation is " + this.op);
		return this.op;
	}

	@Override
	public int getPathLength() {
		// TODO Auto-generated method stub
		return this.pathLen;
	}

	@Override
	public SimplePuzzleState move(int row, int column, Operation op) {
		// TODO Auto-generated method stub
		
		int l = this.curState.length;
		//the following 4 conditionals deal with the OutOfBound exception for row & column
		//if the current tile is at the top row, it cannot MOVEUP
		if(row <= 0 && op == Operation.MOVEUP) {
			return null;
		}
		
		//if the current tile is at the bottom row, it cannot MOVEDOWN
		if(row >= l-1 && op == Operation.MOVEDOWN) {
			return null;
		}
		
		//if the current tile is at the leftmost row, it cannot MOVELEFT
		if(column <= 0 && op == Operation.MOVELEFT) {
			return null;
		}
		
		//if the current tile is at the rightmost row, it cannot MOVERIGHT
		if(column >= l-1 && op == Operation.MOVERIGHT) {
			return null;
		}
		
		//general cases
		switch(op) {
			case MOVERIGHT:
				if (curState[row][column+1] == 0) {
					//System.out.println("MOVE RIGHT is valid");
					SimplePuzzleState nextState = new SimplePuzzleState();
					nextState.parentState = this;
					nextState.op = Operation.MOVERIGHT;
					nextState.pathLen = 1 + this.pathLen;
					
					nextState.curState = new int[l][l];
					for(int i = 0; i < l; ++i) {
						for(int j = 0; j < l; ++j) {
							nextState.curState[i][j] = this.curState[i][j];
						}
					}
					
					nextState.curState[row][column+1] = this.curState[row][column];
					nextState.curState[row][column] = 0;
					
					return nextState;
				}
				break;
				
			case MOVELEFT:
				if (curState[row][column-1] == 0) {
					//System.out.println("MOVE LEFT is valid");
					SimplePuzzleState nextState = new SimplePuzzleState();
					nextState.parentState = this;
					nextState.op = Operation.MOVELEFT;
					nextState.pathLen = 1 + this.pathLen;
					
					nextState.curState = new int[l][l];
					for(int i = 0; i < l; ++i) {
						for(int j = 0; j < l; ++j) {
							nextState.curState[i][j] = this.curState[i][j];
						}
					}
					
					nextState.curState[row][column-1] = this.curState[row][column];
					nextState.curState[row][column] = 0;
					
					return nextState;
				}
				break;
				
			case MOVEUP:
				if (curState[row-1][column] == 0) {
					//System.out.println("MOVE UP is valid");
					SimplePuzzleState nextState = new SimplePuzzleState();
					nextState.parentState = this;
					nextState.op = Operation.MOVEUP;
					nextState.pathLen = 1 + this.pathLen;
					
					nextState.curState = new int[l][l];
					for(int i = 0; i < l; ++i) {
						for(int j = 0; j < l; ++j) {
							nextState.curState[i][j] = this.curState[i][j];
						}
					}
					
					nextState.curState[row-1][column] = this.curState[row][column];
					nextState.curState[row][column] = 0;
					
					return nextState;
				}
				break;
				
			case MOVEDOWN:
				if (curState[row+1][column] == 0) {
					//System.out.println("MOVE DOWN is valid");
					SimplePuzzleState nextState = new SimplePuzzleState();
					nextState.parentState = this;
					nextState.op = Operation.MOVEDOWN;
					nextState.pathLen = 1 + this.pathLen;
					
					nextState.curState = new int[l][l];
					for(int i = 0; i < l; ++i) {
						for(int j = 0; j < l; ++j) {
							nextState.curState[i][j] = this.curState[i][j];
						}
					}
					
					nextState.curState[row+1][column] = this.curState[row][column];
					nextState.curState[row][column] = 0;
					
					return nextState;
				}
				break;
		}
				
		return null;
	}
	
	@Override
	/**
	 * 
	 */
	public SimplePuzzleState drag(int startRow, int startColumn, int endRow, int endColumn) {
		// TODO Auto-generated method stub
		int l = this.curState.length;
		boolean[][] checkBoard = new boolean[l][l];
		ArrayList<CoordState> queue = new ArrayList<>();
		CoordState first = new CoordState(startRow, startColumn, this);
		queue.add(first);
		checkBoard[startRow][startColumn] = true;
		
		while(!queue.isEmpty()) {
			CoordState t = queue.remove(0);
			if(t.x == endRow && t.y == endColumn) {
				return t.state;
			}
			for (int i = 0; i < Operation.values().length; ++i) {
				int x = -1, y = -1;
				switch(Operation.values()[i]) {
					case MOVERIGHT:
						x = t.x; y = t.y + 1;
						break;
					case MOVELEFT:
						x = t.x; y = t.y - 1;
						break;
					case MOVEUP:
						x = t.x - 1; y = t.y;
						break;
					case MOVEDOWN:
						x = t.x + 1; y = t.y;
						break;
				}
				if(x < 0 || x > l-1 || y < 0 || y > l-1) 
					continue;
				if(!checkBoard[x][y]) {			//if not checked
					checkBoard[x][y] = true;	//check it
					CoordState second = new CoordState(x, y, t.state.move(t.x, t.y, Operation.values()[i]));
					if(second.state != null) {
						queue.add(second);
					}
				}
			}
		}
		
		return null;
	}

	/**
	 * Set to initial state first; make # pathLen random operations using move method;
	 * finally return the resulting state;
	 * emptyBase stores the coord of empty slots as TwoTuple class;
	 * moveBase stores the possible Operations that one slot can do 
	 * on the empty slot.
	 */
	@Override
	public SimplePuzzleState shuffleBoard(int pathLength) {
		// TODO Auto-generated method stub
		SimplePuzzleState init = this;
		SimplePuzzleState next = new SimplePuzzleState();
		int emptySlots = 0;
		int l = this.curState.length;
		boolean redo;		//part of repeatChecker
		ArrayList<TwoTuple> emptyBase = new ArrayList<>();
		ArrayList<Operation> moveBase = new ArrayList<>();
		ArrayList<SimplePuzzleState> repeatChecker = new ArrayList<>();
		repeatChecker.add(init);
		
		//count the number of empty slots
		for (int i = 0; i < l; ++i) {
			for (int j = 0; j < l; ++j) {
				if (init.curState[i][j] == 0) {
					emptySlots++;
					TwoTuple tt = new TwoTuple(i, j);
					emptyBase.add(tt);
				}
			}
		}
		
		//processes for creating random numbers: randEmpty and randMove
		//randEmpty is the random number from the emptyBase
		//randMove is the random number from the moveBase
		int randEmpty, randMove;
			
		for(int k = 0; k < pathLength; ++k) {
			redo = false;
			randEmpty = (int) (Math.random() * emptySlots);
			TwoTuple emp = new TwoTuple(-1, -1);
			emp = emptyBase.get(randEmpty);
			
			//initialize possible operations with an arrayList
			moveBase.clear();
			moveBase.add(Operation.MOVERIGHT);
			moveBase.add(Operation.MOVELEFT);
			moveBase.add(Operation.MOVEDOWN);
			moveBase.add(Operation.MOVEUP);
			
			//process moveBase by removing impossible operation,
			//including: 1. edge cases; 2. empty slots neighbors;
			if(emp.x == 0)
				moveBase.remove(Operation.MOVEDOWN);
			else if(init.isEmpty(emp.x - 1, emp.y))
				moveBase.remove(Operation.MOVEDOWN);
			if(emp.x == l-1)
				moveBase.remove(Operation.MOVEUP);
			else if(init.isEmpty(emp.x + 1, emp.y))
				moveBase.remove(Operation.MOVEUP);
			if(emp.y == 0)
				moveBase.remove(Operation.MOVERIGHT);
			else if(init.isEmpty(emp.x, emp.y - 1))
				moveBase.remove(Operation.MOVERIGHT);
			if(emp.y == l-1)
				moveBase.remove(Operation.MOVELEFT);
			else if(init.isEmpty(emp.x, emp.y + 1))
				moveBase.remove(Operation.MOVELEFT);
				
			randMove = (int)(Math.random() * moveBase.size());
			
			//make the movement; newEmpt is the coord for the new empty slot
			int r, c;
			TwoTuple newEmpt = new TwoTuple(-1, -1);
			switch(moveBase.get(randMove)) {
				case MOVERIGHT:
					r = emp.x;
					c = emp.y - 1;
					next = init.move(r, c, Operation.MOVERIGHT);
					newEmpt.setTuple(r, c);
					break;
					
				case MOVELEFT:
					r = emp.x;
					c = emp.y + 1;
					next = init.move(r, c, Operation.MOVELEFT);
					newEmpt.setTuple(r, c);
					break;
					
				case MOVEDOWN:
					r = emp.x - 1;
					c = emp.y;
					next = init.move(r, c, Operation.MOVEDOWN);
					newEmpt.setTuple(r, c);
					break;
					
				case MOVEUP:
					r = emp.x + 1;
					c = emp.y;
					next = init.move(r, c, Operation.MOVEUP);
					newEmpt.setTuple(r, c);
					break;
					
			}
			
			//repetition check
			for(int i = 0; i < repeatChecker.size(); ++i) {
				if(next.equals(repeatChecker.get(i))) {
					k--;
					redo = true;
				}
			}
			if(redo)
				continue;
			
			emptyBase.remove(randEmpty);
			emptyBase.add(newEmpt);
			
			init = next;
			repeatChecker.add(init);
			
		}
		return init;
	}

	@Override
	public boolean isEmpty(int row, int column) {
		// TODO Auto-generated method stub
		if(this.curState[row][column] != 0) {
			return false;
		}
		return true;
	}

	/**
	 * BFS approach:
	 * Start backwards from the initial state;  
	 * while the intended SimplePuzzleState is not found, 
	 * iterate all possible moves through all the slots,
	 * until the state is found
	 */
	@Override
	public PuzzleState getStateWithShortestPath() {
		// TODO Auto-generated method stub
		int emptySlots = 0, l = this.curState.length;
		SimplePuzzleState init = new SimplePuzzleState();
		SimplePuzzleState next = new SimplePuzzleState();
		ArrayList<SimplePuzzleState> queue = new ArrayList<>();
		
		//count the number of empty slots
		for (int i = 0; i < l; ++i) {
			for (int j = 0; j < l; ++j) {
				if (this.curState[i][j] == 0) {
					emptySlots++;
				}
			}
		}
		
		init.setToInitialState(l, emptySlots);
		queue.add(init);
		
		while(!queue.isEmpty()) {
			init = queue.remove(0);
			if(init.equals(this)) {
				return init;
			}
			
			//for each slot (i, j)
			for(int i = 0; i < 4; ++i) {
				for(int j = 0; j < 4; ++j) {
					//for each operation Operation.values()[k]
					for(int k = 0; k < 4; ++k) {
						next = init.move(i, j, Operation.values()[k]);
						if(next != null) {
							queue.add(next);
						}
					}
				}
			}
		}
		
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(curState);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimplePuzzleState other = (SimplePuzzleState) obj;
		if (curState == null && other.curState == null)
			return true;
		else if (curState == null || other.curState == null)
			return false;
		for (int i = 0; i < other.curState.length; ++i) {
			if(!Arrays.equals(curState[i], other.curState[i])) {
				return false;
			}
		}
//		if (!Arrays.deepEquals(curState, other.curState))
//			return false;
		return true;
	}

	//for debug: print the slots
	private void printSlot(SimplePuzzleState s) {
		for(int i = 0; i < s.curState.length; ++i) {
			for(int j = 0; j < s.curState.length; ++j) {
				System.out.print(s.curState[i][j] + " ");
			}
			System.out.println();
		}
	}
	

}
