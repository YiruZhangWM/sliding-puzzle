package edu.wm.cs.cs301.slidingpuzzle;

public class CoordState {
	public int x;
	public int y;
	public SimplePuzzleState state;
	
	public CoordState(int x, int y, SimplePuzzleState state) {
		this.x = x;
		this.y = y;
		this.state = state;
	}
}
