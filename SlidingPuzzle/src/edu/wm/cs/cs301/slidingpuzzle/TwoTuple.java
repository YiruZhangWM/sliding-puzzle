package edu.wm.cs.cs301.slidingpuzzle;

public class TwoTuple {
	public int x;
	public int y;
	
	public TwoTuple(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setTuple(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
